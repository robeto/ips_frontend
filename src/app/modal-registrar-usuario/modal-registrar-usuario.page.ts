import { Component, OnInit, Input } from '@angular/core';
import { NavController, ModalController } from '@ionic/angular';
import { NavParams } from '@ionic/angular';
import { Patient } from '../models/patient';
import { ToastController } from '@ionic/angular';
import { PatientService } from '../services/patient.service';
import { TypeIdService } from '../services/type-id.service';
import { TypeId } from '../models/type-id';


@Component({
  selector: 'app-modal-registrar-usuario',
  templateUrl: './modal-registrar-usuario.page.html',
  styleUrls: ['./modal-registrar-usuario.page.scss'],
})
export class ModalRegistrarUsuarioPage implements OnInit {

  public paciente = new Patient();
  private typeIds: TypeId[] = [];
  public tipoSeleccionado= new TypeId();
  @Input() documento: string;
  

  constructor(public toastController: ToastController, private modalCtrl: ModalController,
     private patientService: PatientService, private typeIdService: TypeIdService) { }

  ngOnInit() {
    this.paciente.numberId = this.documento;
    this.typeIdService.getTypesId().subscribe(
      (result:any) => {
        this.typeIds = result;
        console.log(this.typeIds)

      },
      error => {
        console.log('problemas');
      }
    );
  }


  closeModal() {
    this.modalCtrl.dismiss();
  }
  closeModalSuccess(documento) {
    this.modalCtrl.dismiss({
      'documento': documento
    });


  }

  calcular_edad() {
    var hoy = new Date();
    var fechapaciente = new Date(this.paciente.fechaNacimiento);
    //Valida si la persona es niño 
    if (fechapaciente.getTime() <= hoy.getTime()) {
      var hoy2 = hoy.setMonth(hoy.getMonth() - 216);

      if (fechapaciente.getTime() < new Date(hoy2).getTime()) {
        this.paciente.isChild = false;
        //Valida si la persona es vieja
        var hoyOld = new Date();
        var hoy2Old = hoyOld.setMonth(hoyOld.getMonth() - 800);
        if (fechapaciente.getTime() < new Date(hoy2Old).getTime()) {
          this.paciente.isOld = true;
        } else {
          this.paciente.isOld = false;
        }

      } else {
        this.paciente.isChild = true;
        this.paciente.isOld = false;

      }
    } else {
      this.toastFechaErronea();
      this.paciente.isOld = undefined;
      this.paciente.isChild = undefined;

    }

  }

  agregarUsuario() {
    if (this.paciente.address != undefined && this.paciente.cellPhone != undefined && this.paciente.eps != undefined &&
      this.paciente.lastName != undefined && this.paciente.name != undefined && this.paciente.numberId != undefined && this.paciente.phone != undefined) {
      if (this.patientService.getPatientDocument(this.paciente.numberId) == undefined) {
                //Se valida que ya tenga completada la fecha 
                if(this.paciente.isChild!=undefined && this.paciente.isOld!=undefined){
                  if(this.paciente.isOld || this.paciente.isChild){
                      if(this.paciente.phoneAcudiente!=undefined && this.paciente.acudiente!=undefined){
                        this.paciente.typeId=this.tipoSeleccionado;
                        this.patientService.setPatient(this.paciente);
                        this.closeModalSuccess(this.paciente.numberId);
                      }else{
                       this.toastFormularioIncompleto();
                      }
                  }else{
                    this.paciente.typeId=this.tipoSeleccionado;
                    this.patientService.setPatient(this.paciente);
                    this.closeModalSuccess(this.paciente.numberId);
                  }
               }else{
                 this.toastFormularioIncompleto();
     
               }
      } else {
        this.toastDocumentoExistente();
      }



    } else {
      this.toastFormularioIncompleto();
    }
  }

  async toastFormularioIncompleto() {
    const toast = await this.toastController.create({
      message: 'Debe completar todo el formulario.',
      duration: 2000,
      color: 'danger'
    });
    toast.present();
  }
  async toastFechaErronea() {
    const toast = await this.toastController.create({
      message: 'La fecha de nacimiento no puede ser futura.',
      duration: 2000,
      color: 'danger'
    });
    toast.present();
  }
  async toastDocumentoExistente() {
    const toast = await this.toastController.create({
      message: 'El documento que intentas agregar ya existe.',
      duration: 2000,
      color: 'danger'
    });
    toast.present();
  }
}
