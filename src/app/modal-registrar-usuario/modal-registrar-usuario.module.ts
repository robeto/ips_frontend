import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ModalRegistrarUsuarioPage } from './modal-registrar-usuario.page';

const routes: Routes = [
  {
    path: '',
    component: ModalRegistrarUsuarioPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ModalRegistrarUsuarioPage]
})
export class ModalRegistrarUsuarioPageModule {}
