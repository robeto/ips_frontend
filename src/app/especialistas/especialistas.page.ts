import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { ModalAgregarEspecialistaPage } from '../modal-agregar-especialista/modal-agregar-especialista.page';

import { ModalController } from '@ionic/angular';
import { TherapistService } from '../services/therapist.service';
import { Therapist } from '../models/therapist';

@Component({
  selector: 'app-especialistas',
  templateUrl: './especialistas.page.html',
  styleUrls: ['./especialistas.page.scss'],
})
export class EspecialistasPage implements OnInit {
  
  public terapeutas:Therapist[]=[];
  constructor(public alertController: AlertController,public modalController: ModalController, public terapeutaService:TherapistService) { }

  ngOnInit() {
     this.terapeutas=this.terapeutaService.getAll();
  }


  //Muestra la confirmación para eliminar a un especialista
  async confirmacionEliminar(documento,nombre) {
    const alert = await this.alertController.create({
      header: 'Confirmación',
      message: '¿En verdad deseas eliminar al especialista '+nombre+' ?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Aceptar',
          handler: () => {
            console.log('Aceptar');
          }
        }
      ]
    });

    await alert.present();
  }

  //Muestra el modal para agregar un especialista
      async presentModalAgregar() {
        const modal = await this.modalController.create({
          component: ModalAgregarEspecialistaPage,
          componentProps: { 'accion': 'agregar','titulo':'Agregar especialista'
        }
        });
        return await modal.present();
      }

        //Muestra el modal para agregar un especialista
        async presentModalModificar(idUsuario) {
          const modal = await this.modalController.create({
            component: ModalAgregarEspecialistaPage,
            componentProps: { 'accion': 'modificar','titulo':'Modificar especialista','idUsuario':idUsuario
          }
          });
          return await modal.present();
        }

}
