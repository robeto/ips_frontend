import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EspecialistasPage } from './especialistas.page';

describe('EspecialistasPage', () => {
  let component: EspecialistasPage;
  let fixture: ComponentFixture<EspecialistasPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EspecialistasPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EspecialistasPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
