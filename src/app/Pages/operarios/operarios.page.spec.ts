import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OperariosPage } from './operarios.page';

describe('OperariosPage', () => {
  let component: OperariosPage;
  let fixture: ComponentFixture<OperariosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OperariosPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OperariosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
