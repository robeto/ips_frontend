import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { OperatorService } from '../../services/operator.service';
import { Operator } from '../../models/operator';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-operarios',
  templateUrl: './operarios.page.html',
  styleUrls: ['./operarios.page.scss'],
})
export class OperariosPage implements OnInit {

  Operarios = Array<Operator>();
  constructor(private router: Router, private operservice: OperatorService,
              public toast: ToastController) { }

  ngOnInit() {
    this.LoadOperarios();
  }

  addOperarios = () => {
    this.router.navigate(['/addoperarios']);
  }

  UpdateOperario(name, lastname, phone, cellphone, address, id) {
    this.router.navigate(['/addoperarios/' + name + '/' + lastname + '/' + phone + '/' + cellphone + '/' + address + '/' + id ]);
  }

  DeleteOperario(id) {
    this.operservice.DeleteOperator(id);
    this.Operarios = this.operservice.operators;
    this.Alerta('Operario Eliminado');
  }

  async Alerta(mensaje) {
    const toast = await this.toast.create({
      message: mensaje,
      duration: 2000,
      showCloseButton: true,
      closeButtonText: 'OK'
    });
    toast.present();
  }

  LoadOperarios = () => {
    this.Operarios = this.operservice.operators;
    console.log(this.Operarios);
  }


}
