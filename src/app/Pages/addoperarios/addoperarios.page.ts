import { Component, OnInit } from '@angular/core';
import { OperatorService } from '../../services/operator.service';
import { Operator } from '../../models/operator';
import { ActivatedRoute, RouteReuseStrategy, Router } from '@angular/router';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-addoperarios',
  templateUrl: './addoperarios.page.html',
  styleUrls: ['./addoperarios.page.scss'],
})
export class AddoperariosPage implements OnInit {

  nombre: string;
  apellido: string;
  telefono: string;
  celular: string;
  direccion: string;
  id: number;
  constructor(private operservice: OperatorService ,
              private actroute: ActivatedRoute,
              private router: Router,
              public toast: ToastController) { }

  ngOnInit() {
    this.actroute.params.subscribe((data: any) => {
      this.id = data.id;
      this.nombre = data.name;
      this.apellido = data.lastname;
      this.telefono = data.phone;
      this.celular = data.cellphone;
      this.direccion = data.address;
    });
  }

  Add() {
    const operador = new Operator();
    operador.id = 3;
    operador.typeId = 2;
    operador.name = this.nombre;
    operador.lastName = this.apellido;
    operador.phone = this.telefono;
    operador.cellPhone = this.celular;
    operador.address = this.direccion;
    if (operador.name == undefined || operador.lastName == undefined ||
       operador.phone == undefined || operador.cellPhone == undefined 
       || operador.address == undefined) {
      this.Alerta('Debe ingresar todos los campos!');
      return;
    }
    this.operservice.setOperator(operador);
    this.router.navigate(['/Operarios']);
    this.Alerta('Operario Creado');
  }


  async Alerta(mensaje) {
    const toast = await this.toast.create({
      message: mensaje,
      duration: 2000,
      showCloseButton: true,
      closeButtonText: 'OK'
    });
    toast.present();
  }

  update() {
    const operador = new Operator();
    operador.id = this.id;
    operador.typeId = 2;
    operador.name = this.nombre;
    operador.lastName = this.apellido;
    operador.phone = this.telefono;
    operador.cellPhone = this.celular;
    operador.address = this.direccion;
    this.operservice.UpdateOperator(operador);
    this.router.navigate(['/Operarios']);
    this.Alerta('Operario Modificado');
  }

}
