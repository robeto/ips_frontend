import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddoperariosPage } from './addoperarios.page';

describe('AddoperariosPage', () => {
  let component: AddoperariosPage;
  let fixture: ComponentFixture<AddoperariosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddoperariosPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddoperariosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
