import { Especialidad } from './especialidad'
import { Patient } from './patient'

export class Orden {

    constructor(
        public id?: number,
        public cantidadCitas?: number,
        public createAt?: string,
        public especialidad?: string,
        public fechaSolicitud?: string,
        public observacion?: string,
        public paciente?: Patient,



    ) {}
}
