export class Operator {

    constructor(
        public id?: number,
        public typeId?: number,
        public numberId?: string,
        public name?: string,
        public lastName?: string,
        public phone?: string,
        public cellPhone?: string,
        public address?: string,
    ) {}
}
