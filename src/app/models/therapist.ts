import { Especialidad } from './especialidad';
import { TypeId } from './type-id'

export class Therapist {
    constructor(
        public id?: number,
        public typeId?: TypeId,
        public numberId?: string,
        public profecionalTarjet?: string,
        public name?: string,
        public lastName?: string,
        public phone?: string,
        public cellPhone?: string,
        public address?: string,
        public createAt?:string,
        public especialidades?: Especialidad[]

    ) {}
}
