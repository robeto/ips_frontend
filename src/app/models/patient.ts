import { TypeId } from './type-id'

export class Patient {

    constructor(
        public id?: number,
        public typeId?: TypeId,
        public numberId?: string,
        public name?: string,
        public lastName?: string,
        public phone?: string,
        public cellPhone?: string,
        public address?: string,
        public eps?: string,
        public isPrepagada?: boolean,
        public isChild?: boolean,
        public isOld?: boolean,
        public fechaNacimiento?:string,
        public acudiente?:string,
        public createAt?:string,
        public phoneAcudiente?:string


    ) {}
}



