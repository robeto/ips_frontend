import { Component, OnInit } from '@angular/core';
import { PatientService } from '../services/patient.service';
import { Patient } from '../models/patient';
import { TypeId } from '../models/type-id';

import { AlertController } from '@ionic/angular';
import { ModalRegistrarUsuarioPage } from '../modal-registrar-usuario/modal-registrar-usuario.page';
import { ModalController } from '@ionic/angular';
import { ToastController } from '@ionic/angular';
import { modalController } from '@ionic/core';
import { TypeIdService } from '../services/type-id.service';
import { EspecialidadesService } from '../services/especialidades.service';

import { Orden } from '../models/orden';
import { Especialidad } from '../models/especialidad';
import { OrdenService } from '../services/orden.service';


@Component({
  selector: 'app-orden',
  templateUrl: './orden.page.html',
  styleUrls: ['./orden.page.scss'],
})
export class OrdenPage implements OnInit {

  public paciente = new Patient();
  public typeId = new TypeId();
  private typeIds: TypeId[] = [];
  private especialidades: Especialidad[] = [];

  private orden = new Orden();

  public documento = "";
  constructor(public toastController: ToastController, private patientService: PatientService,
    private typeIdService: TypeIdService, public alertController: AlertController, 
    private modalController: ModalController,private especialidadService: EspecialidadesService, private ordenService: OrdenService) { }

  ngOnInit() {
    this.typeIdService.getTypesId().subscribe(
      (result:any) => {
        this.typeIds = result;

      },
      error => {
        console.log('problemas');
      }
    );

    this.especialidadService.getEspecialidades().subscribe(
      (result:any) => {
        this.especialidades = result;
        console.log(result)

      },
      error => {
        console.log('problemas');
      }
    );
  }


  existe_usuario() {
    if (this.documento != "") {
      var pacienteEncontrado = new Patient();
      pacienteEncontrado = this.patientService.getPatientDocument(this.documento);

      if (pacienteEncontrado != undefined) {
        this.paciente = new Patient();
        this.paciente = pacienteEncontrado;
        this.typeId = this.paciente.typeId;

      } else {
        this.paciente = new Patient(0, this.typeId, '', '', '', '', '', '', '', true, false, false);
        this.confirmacionCrearPaciente(this.documento);
      }
    } else {
      this.toastDocumentoVacio();
    }

  }

  agregar_orden(){
    if(this.orden!=undefined &&  
      this.orden.cantidadCitas!=undefined && this.orden.especialidad!=undefined){
       if(this.patientService.getPatientDocument(this.paciente.numberId)!=undefined){
           this.orden.paciente=this.paciente;
           let fecha = new Date();
           this.orden.fechaSolicitud=fecha.toString();
           this.ordenService.setOrden(this.orden);
          }else{
           this.toastUsuarioInexistente();
       }
    }else{
      this.toastErrorOrden();
    }
  }


  async toastErrorOrden() {
    const toast = await this.toastController.create({
      message: 'Debe completar toda la información de la orden.',
      duration: 2000,
      color: 'danger'
    });
    toast.present();
  }

  async toastUsuarioInexistente() {
    const toast = await this.toastController.create({
      message: 'No se encontró el paciente indicado.',
      duration: 2000,
      color: 'danger'
    });
    toast.present();
  }


  //Muestra la confirmación para crear un usuario
  async confirmacionCrearPaciente(documento) {
    const alert = await this.alertController.create({
      header: 'Documento inexistente',
      message: '<strong>¿deseas crear al paciente?</strong>',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Aceptar',
          handler: () => {
            this.presentModalAgregar(documento);
          }
        }
      ]
    });

    await alert.present();
  }

  async toastDocumentoVacio() {
    const toast = await this.toastController.create({
      message: 'Debe indicar un documento.',
      duration: 2000,
      color: 'danger'
    });
    toast.present();
  }

  //Muestra el modal para agregar un especialista

  async presentModalAgregar(documento) {
    
    const modal = await this.modalController.create({
      component: ModalRegistrarUsuarioPage,
      componentProps: {
        'documento': documento
      }
    });
    modal.onDidDismiss()
    .then((data) => {
      const valor = data['data']; // Here's your selected user!
      this.documento=valor['documento'];
      this.existe_usuario();
    });
 
    return await modal.present();
  }




}