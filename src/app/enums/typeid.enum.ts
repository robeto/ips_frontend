export enum TypeIdentifications {
    cedula = 1,
    tarjetaIdentidad = 2,
    cedulaExtranjeria = 3,
    carnetDiplomatico = 4
}
