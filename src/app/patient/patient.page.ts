import { PatientService } from '../services/patient.service';
import { Patient } from '../models/patient';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-patient',
  templateUrl: './patient.page.html',
  styleUrls: ['./patient.page.scss'],
})
export class PatientPage implements OnInit {

  tiposId: string[] = ['Cedula', 'Tarjeta de identidad', 'Carnet diplomatico', 'Cedula de extranjeria'];
  eps: string[] = ['Sanitas', 'Compensar', 'Cruz Roja', 'Colsubsidio', 'Cafam'];
  public Patients: Patient[] = [];
  
  constructor( private patietnService: PatientService ) { }

  ngOnInit() {
  }

  createPatient( patient: Patient ) {
  
  }
}
