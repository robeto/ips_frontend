import { MenuController } from '@ionic/angular';
import { Router } from '@angular/router';
import { Operator } from './../models/operator';
import { OperatorService } from './../services/operator.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  constructor(public menuCtrl: MenuController,private router: Router) { }

  ngOnInit() {
    this.ionViewWillEnter();
  }

  ionViewWillEnter() {
    this.menuCtrl.enable(false);
  }
  iniciar_sesion(){
    location.href="";

  }
}
