import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then(m => m.HomePageModule)
  },
  {
    path: 'list',
    loadChildren: () => import('./list/list.module').then(m => m.ListPageModule)
  },
  {
    path: 'especialistas',
    loadChildren: () => import('./especialistas/especialistas.module').then(m => m.EspecialistasPageModule) 
  },
  { path: 'modal-agregar-especialista',
    loadChildren: () => import('./modal-agregar-especialista/modal-agregar-especialista.module')
                              .then(m => m.ModalAgregarEspecialistaPageModule)},
  {
    path: 'list',
    loadChildren: () => import('./list/list.module').then(m => m.ListPageModule)
  },
    { path: 'paciente',
    loadChildren: () => import('./patient/patient.module').then(m => m.PatientPageModule)
  },
  { path: 'ver-pacientes',
    loadChildren: () => import('./show-patient/show-patient.module').then( m => m.ShowPatientPageModule)
  },
  {
    path: 'Operarios',
    loadChildren: () => import('./Pages/operarios/operarios.module').then(m => m.OperariosPageModule)
  },
  { path: 'addoperarios',
    loadChildren: () => import('./Pages/addoperarios/addoperarios.module').then( m => m.AddoperariosPageModule)
  },
  { path: 'addoperarios/:name/:lastname/:phone/:cellphone/:address/:id',
    loadChildren: () => import('./Pages/addoperarios/addoperarios.module').then( m => m.AddoperariosPageModule)
  },
  { path: 'especialistas', loadChildren: './especialistas/especialistas.module#EspecialistasPageModule' },
  { path: 'modal-agregar-especialista', loadChildren: './modal-agregar-especialista/modal-agregar-especialista.module#ModalAgregarEspecialistaPageModule' },
  { path: 'orden', loadChildren: './orden/orden.module#OrdenPageModule' },
  { path: 'modal-registrar-usuario', loadChildren: './modal-registrar-usuario/modal-registrar-usuario.module#ModalRegistrarUsuarioPageModule' },
  { path: 'login', loadChildren: './login/login.module#LoginPageModule' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})

export class AppRoutingModule {}

