import { Component, OnInit,Input } from '@angular/core';
import { NavController,ModalController } from '@ionic/angular';
import { NavParams } from '@ionic/angular';
import { TherapistService } from '../services/therapist.service';
import { Therapist } from '../models/therapist';

@Component({
  selector: 'app-modal-agregar-especialista',
  templateUrl: './modal-agregar-especialista.page.html',
  styleUrls: ['./modal-agregar-especialista.page.scss'],
})
export class ModalAgregarEspecialistaPage implements OnInit {

  @Input() accion: string;
  @Input() titulo: string;
  @Input() idUsuario:number;
  public terapeuta= new Therapist();
  constructor(private nav:NavController,private modalCtrl:ModalController,navParams: NavParams,private terapeutaService:TherapistService) { }

  ngOnInit() {
    if(this.idUsuario > 0){
         this.terapeuta = this.terapeutaService.getTherapist(this.idUsuario);
         console.log(this.terapeuta.typeId);
    }
  }

  closeModal() {
    this.modalCtrl.dismiss();
    }
}
