import { OperatorService } from './services/operator.service';
import { ServicesModule } from './services/services.module';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { ModalAgregarEspecialistaPage } from './modal-agregar-especialista/modal-agregar-especialista.page';
import { FormsModule } from '@angular/forms';
import { ModalRegistrarUsuarioPage } from './modal-registrar-usuario/modal-registrar-usuario.page';
import {HttpClientModule} from '@angular/common/http';



@NgModule({
  declarations: [AppComponent,ModalAgregarEspecialistaPage,ModalRegistrarUsuarioPage],
  entryComponents: [ModalAgregarEspecialistaPage,ModalRegistrarUsuarioPage],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    ServicesModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
