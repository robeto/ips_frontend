import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowPatientPage } from './show-patient.page';

describe('ShowPatientPage', () => {
  let component: ShowPatientPage;
  let fixture: ComponentFixture<ShowPatientPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowPatientPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowPatientPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
