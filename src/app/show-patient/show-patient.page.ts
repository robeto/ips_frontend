import { PatientService } from '../services/patient.service';
import { Patient } from '../models/patient';
import { Component, OnInit } from '@angular/core';
@Component({
  selector: 'app-show-patient',
  templateUrl: './show-patient.page.html',
  styleUrls: ['./show-patient.page.scss'],
})
export class ShowPatientPage implements OnInit {

  public patients: Patient[] = [];
  constructor(private patientService: PatientService) { }

  ngOnInit() {
    this.patients = this.patientService.getPatients();
    console.log(this.patients);
  }

}
