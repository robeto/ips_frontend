import { Patient } from './../models/patient';
import { Injectable } from '@angular/core';
import { StaticDataSource } from './static.datasource';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class PatientService {
    private patients: Patient[] = [];

    constructor(private dataSource: StaticDataSource,private http: HttpClient) {
        dataSource.getPatients().subscribe(data => {
            this.patients = data;
        });
    }

    getPatient(id: number): Patient {
        return this.patients.find(p => p.id === id);
    }

    getPatientDocument(documento: String): Patient {
        return this.patients.find(p => p.numberId === documento);
    }
    obtenerPacienteDocumento(documento:String){
        return this.http.get('https://api-ips.herokuapp.com/api/paciente/'+documento);
    }

    setPatient(patient: Patient): void {
        patient.id=0;
        patient.createAt="";
        patient.isPrepagada=false;

        this.http.post('https://api-ips.herokuapp.com/api/pacientes',patient).subscribe((response: Response) =>{
            console.log (response.json());
    });
        this.patients.push(patient);
    }

    getPatients(): Patient[] {
        return this.patients;
    }
}