import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class EspecialidadesService {

  constructor(private http: HttpClient) { }


  getEspecialidades(){
    return this.http.get('https://api-ips.herokuapp.com/api/specialties');
   }
}
