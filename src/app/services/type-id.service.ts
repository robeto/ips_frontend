import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { TypeId } from '../models/type-id';


@Injectable({
  providedIn: 'root'
})


export class TypeIdService {
  private typeIds: TypeId[] = [];



  constructor(private http: HttpClient) {
  
    this.http.get('https://api-ips.herokuapp.com/api/type-id').subscribe(
      (result:any) => {
        this.typeIds = result;
     

      },
      error => {
        console.log('problemas');
      }
    )
    
   }

  getTypesId(){
   return this.http.get('https://api-ips.herokuapp.com/api/type-id');
  }

}

