import { TherapistService } from './therapist.service';
import { OperatorService } from './operator.service';
import { PatientService } from './patient.service';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { StaticDataSource } from './static.datasource';



@NgModule({
  imports: [HttpClientModule],
  providers: [PatientService,OperatorService,StaticDataSource,TherapistService]
})
export class ServicesModule { }
