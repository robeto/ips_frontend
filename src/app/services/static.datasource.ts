import { Therapist } from './../models/therapist';
import { Operator } from './../models/operator';
import { Patient } from './../models/patient';
import { Injectable } from '@angular/core';
import { Observable, from } from 'rxjs';
import { TypeId } from '../models/type-id';

@Injectable()
export class StaticDataSource {

    private typeId = new TypeId(1,'Cedula');

    private patients: Patient[] = [
       new Patient(1, this.typeId, '101536985' , 'lorem', 'impsu', '6985245', '301252385', 'calle falsa 123', 'sanitas', true, false, false),
       new Patient(2, this.typeId, '101536996' , 'andres', 'free', '6915245', '301252985', 'calle falsa 456', 'compensar', true, false, false),
       new Patient(3, this.typeId, '101536632' , 'dayan', 'olaya', '3985245', '311220985', 'calle falsa 789', 'cafam', false, true, false),
       new Patient(4, this.typeId, '101531542' , 'camilo', 'fique', '9985545', '31125585', 'calle falsa 122', 'colsubsidio', false, false, false),
       new Patient(5, this.typeId, '101536258' , 'beto', 'garcia', '2985245', '3112512585', 'calle falsa 133', 'cafe salud', false, false, true),
       new Patient(6, this.typeId, '101126985' , 'lorena', 'appellido', '6885215', '312256985', 'calle falsa 143', 'sanitas', true, false, true),
       new Patient(7, this.typeId, '115536985' , 'daniela', 'lopez', '5925245', '310256985', 'calle falsa 113', 'sanitas', false, false, false),
       new Patient(8, this.typeId, '198536985' , 'ruby', 'rails', '6985235', '312256985', 'calle falsa 111', 'sanitas', false, true, false),
       new Patient(9, this.typeId, '102536985' , 'java', 'spring', '6915205', '301256985', 'calle falsa 122', 'sanitas', false, false, false),
       new Patient(10, this.typeId, '136535985' , 'phyton', 'flats', '3985245', '315256985', 'calle falsa 126', 'sanitas', false, true, false),
    ];
    private operator: Operator[] = [
        new Operator(1, 1, '78425632', 'operator 1', 'lastOper', '9563252', '311256985', 'calle falsa sprinfield'),
        new Operator(2, 1, '80425632', 'operator 2', 'lastOper 2', '9565852', '32126985', 'calle falsa quajok')
    ];
    private therapist: Therapist[] = [
        new Therapist(1, this.typeId, '108496587', '11814965', 'Claudia', 'mujer', '6589421', '32158965', 'calle 1234','prueba@prueba.com'),
        new Therapist(2, this.typeId, '101496587', '119162965', 'Patricia', 'lar', '7589421', '31158965', 'calle 1526','prueba@prueba.com'),
        new Therapist(3, this.typeId, '115496587', '10211965', 'Pedro', 'cree', '7580421', '30158985', 'calle 1291','prueba@prueba.com'),
        new Therapist(4, this.typeId, '136496587', '11313965', 'Pablo', 'travel', '5512421', '30558965', 'calle 1394','prueba@prueba.com')
    ];

    getPatients(): Observable<Patient[]> {
        return from([this.patients]);
    }

    getOperators(): Observable<Operator[]> {
        return from([this.operator]);
    }

    getTherapist(): Observable<Therapist[]> {
        return from([this.therapist]);
    }
}
