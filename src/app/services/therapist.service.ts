import { Therapist } from './../models/therapist';
import { Patient } from './../models/patient';
import { Injectable } from '@angular/core';
import { StaticDataSource } from './static.datasource';

@Injectable()
export class TherapistService {
    private therapist: Therapist[] = [];

    constructor(private dataSource: StaticDataSource) {
        dataSource.getTherapist().subscribe(data => {
            this.therapist = data;
        });
    }

    getTherapist(id: number): Therapist {
        return this.therapist.find(p => p.id === id);
    }

    setTherapist(therapist: Therapist): void {
        this.therapist.push(therapist);
    }

    getAll(): Therapist []{
        return this.therapist;
    }
}