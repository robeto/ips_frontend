import { Operator } from './../models/operator';
import { Injectable } from '@angular/core';
import { StaticDataSource } from './static.datasource';

@Injectable()
export class OperatorService {
    public operators: Operator[] = [];
    constructor(private dataSource: StaticDataSource) {
        dataSource.getOperators().subscribe(data => {
            this.operators = data;
        });
    }

    getOperator(id: number): Operator {
        return this.operators.find(p => p.id === id);
    }

    setOperator(operator: Operator): void {
        this.operators.push(operator);
    }

    public getOperators(): Operator[] {
        return this.operators;
    }

    UpdateOperator(operator: Operator): void {
        this.operators.forEach(element => {
            if (element.id == operator.id) {
                element.name = operator.name;
                element.lastName = operator.lastName;
                element.phone = operator.phone;
                element.cellPhone = operator.cellPhone;
                element.address = operator.address;
            }
        });
     }
 
     DeleteOperator(id: number): void {
        const obj = this.operators.findIndex(x => x.id == id);
        this.operators.splice(obj, 1);
     }
}
