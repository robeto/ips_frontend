import { TestBed } from '@angular/core/testing';

import { TypeIdService } from './type-id.service';

describe('TypeIdService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TypeIdService = TestBed.get(TypeIdService);
    expect(service).toBeTruthy();
  });
});
