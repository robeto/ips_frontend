import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Orden } from '../models/orden';


@Injectable({
  providedIn: 'root'
})
export class OrdenService {

  private ordenes: Orden[] = [];

  constructor(private http: HttpClient) { }

  setOrden(orden:Orden){
    orden.id= Math.round(Math.random()*1000);
    this.ordenes.push(orden);
    console.log(this.ordenes);
  }
}
